# Application of the MuZero Algorithm to a Visually Rich Self-Driving Environment

## Intructions

Based on the [muzero-general GitHub repo](https://github.com/werner-duvaud/muzero-general). See that repo for instruction on how to run.

## Project

This project is a study into the performance of the MuZero algorithm on the OpenAI Gym environment [CarRacing-v0](https://gym.openai.com/envs/CarRacing-v0/). The project was performed in January 2021 for the course Pattern Recognition at the Utrecht University.

## Authors

- Mats Gottenbos
- Jason van den Hurk
- Menno Klunder
- Bart van Schie
